#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "bot.h"

double myx,myy,mex,mey;




int dist(const void *a, const void *b)
{
    double ax = (*(struct food *)a).x;
    double ay = (*(struct food *)a).y;
    double bx = (*(struct food *)b).x;
    double by = (*(struct food *)b).y;
    
    double dista = sqrt((myx - ax)*(myx - ax) + (myy - ay)*(myy - ay)); 
    double distb = sqrt((myx - bx)*(myx - bx) + (myy - by)*(myy - by));
    return dista - distb;
}

int playerDistance(const void *a, const void *b)
{
    double ax = (*(struct player *)a).x;
    double ay = (*(struct player *)a).y;
    double bx = (*(struct player *)b).x;
    double by = (*(struct player *)b).y;
  
    double player_dista = sqrt((mex - ax)*(mex - ax) + (mey - ay)*(mey - ay));
    double player_distb = sqrt((mex - bx)*(mex - bx) + (mey - by)*(mey - by));
    
    return player_dista - player_distb;
  
}

int virusDistance(const void *a, const void *b)
{
    double vx = (*(struct cell *)a).x;
    double vy = (*(struct cell *)a).y;
    double zx = (*(struct cell *)b).x;
    double zy = (*(struct cell *)b).y;
    
    double virus_dista = sqrt((mex - vx)*(mex - vx)+( mey - vy)*(mey - vy));
    double virus_distb = sqrt((mex - zx)*(mex - zx)+( mey - zy)*(mey - zy));
    
    return virus_dista - virus_distb;
    
}

struct action playerMove(struct player me, 
                         struct player * players, int nplayers,
                         struct food * foods, int nfood,
                         struct cell * virii, int nvirii,
                         struct cell * mass, int nmass)
{
    struct action act;
    
    myx = me.x;
    myy = me.y;
    
    mex = me.x;
    mey = me.y;
    
    qsort(foods, nfood, sizeof(struct food), dist);
    qsort(players,nplayers,sizeof(struct player),playerDistance);
    qsort(virii,nvirii,sizeof(struct cell),virusDistance);
    
    // Move to the lower-right
    if(virii[0].x <= 30 && virii[0].y <= 30)
    {
        act.dx = -30;
        act.dy = -30;
    }
    else
    {
    act.dx = foods[0].x - me.x;
    act.dy = foods[0].y - me.y;
    }
 

   if(players[0].x <= 30 && players[0].y <= 30)
   {
      act.split = 1;
   }
      
    act.fire = 0;
    return act;
}